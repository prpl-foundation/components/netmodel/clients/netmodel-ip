/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxs/amxs.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <netmodel/client.h>
#include <netmodel/mib.h>
#include "mib_ip.h"

#define ME "ip_mib"
#define MOD "ip"
#define UNUSED __attribute__((unused))
#define string_empty(x) ((x == NULL) || (*x == '\0'))

static void update_flag(const char* netmodel_interface_path, const char* flag, bool set) {
    amxc_var_t args;
    amxc_var_t ret;
    const char* function = set ? "setFlag" : "clearFlag";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);

    if(AMXB_STATUS_OK != amxb_call(netmodel_get_amxb_bus(), netmodel_interface_path, function, &args, &ret, 5)) {
        SAH_TRACEZ_ERROR(ME, "Failed to call %s.%s(flag=\"%s\")", netmodel_interface_path, function, flag);
        goto exit;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static amxs_status_t ipv4enable_action_cb(const amxs_sync_entry_t* sync_entry UNUSED, amxs_sync_direction_t direction, amxc_var_t* data, void* priv UNUSED) {
    const char* netmodel_intf = GET_CHAR(data, "path");

    when_false((direction == amxs_sync_a_to_b), exit);

    update_flag(netmodel_intf, "ipv4", GETP_BOOL(data, "parameters.IPv4Enable"));
exit:
    return amxs_status_ok;
}

static amxs_status_t ipv6enable_action_cb(const amxs_sync_entry_t* sync_entry UNUSED, amxs_sync_direction_t direction, amxc_var_t* data, void* priv UNUSED) {
    const char* netmodel_intf = GET_CHAR(data, "path");

    when_false((direction == amxs_sync_a_to_b), exit);

    update_flag(netmodel_intf, "ipv6", GETP_BOOL(data, "parameters.IPv6Enable"));
exit:
    return amxs_status_ok;
}

static bool search_for_valid_address(const char* netmodel_intf, const char* address, bool ipv4, bool* has_address) {
    amxc_var_t ret;
    amxc_var_t* matches = NULL;
    amxc_var_t* var = NULL;
    amxc_string_t search_path_str;
    const char* search_path = NULL;
    bool retval = false;

    amxc_var_init(&ret);
    amxc_var_set_type(&ret, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&search_path_str, 0);
    amxc_string_setf(&search_path_str, "%s.IPv%dAddress.[Status == 'Enabled'].", netmodel_intf, ipv4 ? 4 : 6);
    search_path = amxc_string_get(&search_path_str, 0);

    if(AMXB_STATUS_OK != amxb_get(netmodel_get_amxb_bus(), search_path, 0, &ret, 5)) {
        SAH_TRACEZ_ERROR(ME, "Can't update ipv%d-up flag for %s", ipv4 ? 4 : 6, netmodel_intf);
        goto exit;
    }

    matches = GETP_ARG(&ret, "0");
    var = amxc_var_take_key(matches, address); // when called in amxs action cb, the previous status could still be Enabled
    amxc_var_delete(&var);
    *has_address = amxc_var_get_first(matches) != NULL;

    retval = true;

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&search_path_str);
    return retval;
}

static amxs_status_t ipvx_status_action_cb(bool ipv4, const amxs_sync_entry_t* sync_entry, amxs_sync_direction_t direction, amxc_var_t* data, void* priv) {
    amxs_sync_ctx_t* ctx = (amxs_sync_ctx_t*) priv;
    const char* netmodel_intf = ctx->b;
    const char* address = GET_CHAR(data, "path");
    const char* status = GETP_CHAR(data, "parameters.Status");
    bool has_address = false;

    when_false((direction == amxs_sync_a_to_b), exit);

    has_address = !string_empty(status) && (strcmp(status, "Enabled") == 0);
    if(!has_address) {
        when_false(search_for_valid_address(netmodel_intf, address, ipv4, &has_address), exit);
    }

    update_flag(netmodel_intf, ipv4 ? "ipv4-up" : "ipv6-up", has_address);

exit:
    return amxs_sync_param_copy_action_cb(sync_entry, direction, data, priv);
}

static amxs_status_t ipv4_status_action_cb(const amxs_sync_entry_t* sync_entry, amxs_sync_direction_t direction, amxc_var_t* data, void* priv) {
    return ipvx_status_action_cb(true, sync_entry, direction, data, priv);
}

static amxs_status_t ipv6_status_action_cb(const amxs_sync_entry_t* sync_entry, amxs_sync_direction_t direction, amxc_var_t* data, void* priv) {
    return ipvx_status_action_cb(false, sync_entry, direction, data, priv);
}

static amxs_status_t add_parameters(amxs_sync_ctx_t* ctx) {
    amxs_status_t ret = amxs_status_unknown_error;
    amxs_sync_object_t* obj = NULL;

    ret = amxs_sync_ctx_add_new_param(ctx, "IPv4Enable", "IPv4Enable", AMXS_SYNC_ONLY_A_TO_B, amxs_sync_param_copy_trans_cb, ipv4enable_action_cb, NULL);
    ret |= amxs_sync_ctx_add_new_param(ctx, "IPv6Enable", "IPv6Enable", AMXS_SYNC_ONLY_A_TO_B, amxs_sync_param_copy_trans_cb, ipv6enable_action_cb, NULL);
    ret |= amxs_sync_ctx_add_new_copy_param(ctx, "IPv6AddressDelegate", "IPv6AddressDelegate", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_ctx_add_new_copy_param(ctx, "MaxMTUSize", "MTU", AMXS_SYNC_DEFAULT);

    ret |= amxs_sync_object_new_copy(&obj, "IPv4Address.", "IPv4Address.", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "IPAddress", "IPAddress", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Enable", "Enable", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Scope", "Scope", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Flags", "Flags", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "TypeFlags", "TypeFlags", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Peer", "Peer", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "PrefixLen", "PrefixLen", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_param(obj, "Status", "Status", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, ipv4_status_action_cb, ctx);
    ret |= amxs_sync_object_add_new_copy_param(obj, "SubnetMask", "SubnetMask", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_ctx_add_object(ctx, obj);

    ret |= amxs_sync_object_new_copy(&obj, "IPv6Address.", "IPv6Address.", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "IPAddress", "IPAddress", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Enable", "Enable", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Scope", "Scope", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Flags", "Flags", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "TypeFlags", "TypeFlags", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "PrefixLen", "PrefixLen", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Prefix", "Prefix", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Peer", "Peer", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_param(obj, "Status", "Status", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, ipv6_status_action_cb, ctx);
    ret |= amxs_sync_ctx_add_object(ctx, obj);

    ret |= amxs_sync_object_new_copy(&obj, "IPv6Prefix.", "IPv6Prefix.", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Enable", "Enable", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Prefix", "Prefix", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Status", "Status", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "PrefixStatus", "PrefixStatus", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Origin", "Origin", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "StaticType", "StaticType", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "ValidLifetime", "ValidLifetime", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "PreferredLifetime", "PreferredLifetime", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "RelativeValidLifetime", "RelativeValidLifetime", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "RelativePreferredLifetime", "RelativePreferredLifetime", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "Autonomous", "Autonomous", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_object_add_new_copy_param(obj, "OnLink", "OnLink", AMXS_SYNC_DEFAULT);
    ret |= amxs_sync_ctx_add_object(ctx, obj);

    return ret;
}

static netmodel_mib_t mib = {
    .name = ME,
    .root = "IP.",
    .add_sync_parameters = add_parameters,
};

int mib_added(UNUSED const char* const function_name,
              amxc_var_t* args,
              UNUSED amxc_var_t* ret) {
    netmodel_subscribe(args, &mib);

    return 0;
}

int mib_removed(UNUSED const char* const function_name,
                amxc_var_t* args,
                UNUSED amxc_var_t* ret) {
    netmodel_unsubscribe(args, &mib);
    update_flag(GET_CHAR(args, "object"), "ipv4 ipv6 ipv4-up ipv6-up", false);

    return 0;
}

static AMXM_CONSTRUCTOR mib_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "Mib init triggered");

    amxm_module_register(&mod, so, MOD);
    amxm_module_add_function(mod, "mib-added", mib_added);
    amxm_module_add_function(mod, "mib-removed", mib_removed);

    netmodel_initialize();

    return 0;
}

static AMXM_DESTRUCTOR mib_clean(void) {
    SAH_TRACEZ_INFO(ME, "Mib clean triggered");

    netmodel_cleanup();

    return 0;
}
