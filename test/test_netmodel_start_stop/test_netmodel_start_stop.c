/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <netmodel/client.h>

#include "mib_ip.h"

#include "test_netmodel_start_stop.h"
#include "dummy_be.h"

static amxb_bus_ctx_t* bus_ctx = NULL;
static amxc_var_t flags;

static bool test_flag_is_set(const char* flag) {
    return amxc_var_get_key(&flags, flag, AMXC_VAR_FLAG_DEFAULT) != NULL;
}

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static amxd_status_t _setFlag(UNUSED amxd_object_t* object,
                              UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    amxc_string_t str;
    amxc_var_t var_list;

    amxc_var_init(&var_list);
    amxc_string_init(&str, 0);
    amxc_string_set(&str, GET_CHAR(args, "flag"));
    amxc_string_ssv_to_var(&str, &var_list, NULL);

    amxc_var_for_each(var, &var_list) {
        const char* flag = amxc_var_constcast(cstring_t, var);
        amxc_var_add_key(cstring_t, &flags, flag, "something");
    }

    amxc_var_clean(&var_list);
    amxc_string_clean(&str);
    return amxd_status_ok;
}

static amxd_status_t _clearFlag(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    amxc_string_t str;
    amxc_var_t var_list;

    amxc_var_init(&var_list);
    amxc_string_init(&str, 0);
    amxc_string_set(&str, GET_CHAR(args, "flag"));
    amxc_string_ssv_to_var(&str, &var_list, NULL);

    amxc_var_for_each(var, &var_list) {
        amxc_var_t* var2 = amxc_var_take_key(&flags, amxc_var_constcast(cstring_t, var));
        amxc_var_delete(&var2);
    }

    amxc_var_clean(&var_list);
    amxc_string_clean(&str);

    return amxd_status_ok;
}

int test_nm_setup(UNUSED void** state) {
    amxo_parser_t* parser = NULL;

    amxc_var_init(&flags);
    amxc_var_set_type(&flags, AMXC_VAR_ID_HTABLE);

    assert_int_equal(test_register_dummy_be(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);

    parser = test_get_parser();
    amxo_resolver_ftab_add(parser, "setFlag", AMXO_FUNC(_setFlag));
    amxo_resolver_ftab_add(parser, "clearFlag", AMXO_FUNC(_clearFlag));

    assert_int_equal(test_load_dummy_remote("ip.odl"), 0);
    assert_int_equal(test_load_dummy_remote("netmodel.odl"), 0);

    assert_true(netmodel_initialize());

    return 0;
}

int test_nm_teardown(UNUSED void** state) {
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    assert_int_equal(test_unregister_dummy_be(), 0);
    netmodel_cleanup();

    amxc_var_clean(&flags);

    return 0;
}

void test_can_start_plugin(UNUSED void** state) {
    amxc_var_t data;
    amxd_trans_t trans;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.ip-lan.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");

    mib_added(NULL, &data, NULL);

    assert_true(test_flag_is_set("ipv4"));
    assert_true(test_flag_is_set("ipv6"));

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.");
    amxd_trans_set_value(bool, &trans, "IPv4Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, test_get_dm()), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_false(test_flag_is_set("ipv4"));
    assert_true(test_flag_is_set("ipv6"));

    amxd_trans_select_pathf(&trans, "IP.Interface.lan.");
    amxd_trans_set_value(bool, &trans, "IPv4Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, test_get_dm()), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv4"));
    assert_true(test_flag_is_set("ipv6"));

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.");
    amxd_trans_set_value(bool, &trans, "IPv6Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, test_get_dm()), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv4"));
    assert_false(test_flag_is_set("ipv6"));

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.lan.");
    amxd_trans_set_value(bool, &trans, "IPv6Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, test_get_dm()), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv4"));
    assert_true(test_flag_is_set("ipv6"));

    amxc_var_clean(&data);
}

void test_ipv4up_flag_is_set_and_cleared(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* a1 = NULL;
    amxd_object_t* a2 = NULL;

    assert_false(test_flag_is_set("ipv4-up"));

    a1 = amxd_dm_findf(dm, "IP.Interface.lan.IPv4Address.1.");
    assert_non_null(a1);
    a2 = amxd_dm_findf(dm, "IP.Interface.lan.IPv4Address.2.");
    assert_non_null(a2);

    // a1.Status == Enabled and a2.Status == Disabled, expect flag == ipv4-up
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, a1);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv4-up"));

    // a1.Status == Enabled and a2.Status == Enabled, expect flag == ipv4-up
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, a2);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv4-up"));

    // a1.Status == Disabled and a2.Status == Enabled, expect flag == ipv4-up
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, a1);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv4-up"));

    // a1.Status == Disabled and a2.Status == Disabled, expect flag == ''
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, a2);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_false(test_flag_is_set("ipv4-up"));
}

void test_ipv6up_flag_is_set_and_cleared(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* a1 = NULL;
    amxd_object_t* a2 = NULL;

    assert_false(test_flag_is_set("ipv6-up"));

    a1 = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.1.");
    assert_non_null(a1);
    a2 = amxd_dm_findf(dm, "IP.Interface.lan.IPv6Address.2.");
    assert_non_null(a2);

    // a1.Status == Enabled and a2.Status == Disabled, expect flag == ipv6-up
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, a1);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv6-up"));

    // a1.Status == Enabled and a2.Status == Enabled, expect flag == ipv6-up
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, a2);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv6-up"));

    // a1.Status == Disabled and a2.Status == Enabled, expect flag == ipv6-up
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, a1);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_true(test_flag_is_set("ipv6-up"));

    // a1.Status == Disabled and a2.Status == Disabled, expect flag == ''
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, a2);
    amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    assert_false(test_flag_is_set("ipv6-up"));
}

void test_can_stop_plugin(UNUSED void** state) {
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.ip-lan.");

    mib_removed(NULL, &data, NULL);

    assert_false(test_flag_is_set("ipv4"));
    assert_false(test_flag_is_set("ipv6"));
    assert_false(test_flag_is_set("ipv4-up"));
    assert_false(test_flag_is_set("ipv6-up"));

    amxc_var_clean(&data);
}
