# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.4.1 - 2024-12-03(11:01:21 +0000)

### Other

- - [routeradvertisement] A and L flags must be set by default

## Release v1.4.0 - 2023-10-09(07:07:16 +0000)

### New

- make it possible to query MTU

## Release v1.3.1 - 2023-09-21(11:25:46 +0000)

### Fixes

- [Fut][Random]Youtube and facebook are not accessible with a browser (firefox) - no RA on LAN

## Release v1.3.0 - 2023-04-12(12:08:48 +0000)

### New

- Support IPv6 unnumbered mode

## Release v1.2.1 - 2023-02-28(18:11:42 +0000)

### Fixes

- Return the relative lifetimes in getIPv6Prefix result

## Release v1.2.0 - 2023-02-23(14:30:37 +0000)

### New

- Box sends 2 ICMPv6 RA when a RS is received on LAN

## Release v1.1.2 - 2022-11-28(15:34:25 +0000)

### Other

- [NetModel] Update copyright information

## Release v1.1.1 - 2022-04-21(15:44:19 +0000)

### Fixes

- NetModel getAddr asynchronous query not working

## Release v1.1.0 - 2022-04-19(11:58:42 +0000)

### New

- [netmodel] Support of prefix queries in netmodel/libnetmodel

## Release v1.0.0 - 2022-02-28(10:58:55 +0000)

### Breaking

- Use amxm to start/stop syncing netmodel clients

## Release v0.3.1 - 2022-02-15(07:52:19 +0000)

### Fixes

- only the NetDev client should synchronize NetDevName

## Release v0.3.0 - 2022-02-04(07:16:42 +0000)

### New

- mib_ip should set flags "ipv4-up" and "ipv6-up"

## Release v0.2.2 - 2022-01-27(12:28:34 +0000)

### Fixes

- wrong mib can be unsubscribed when netmodel clients share a netmodel interface

## Release v0.2.1 - 2022-01-21(16:25:22 +0000)

### Changes

- expand libnetmodel for mibs/netmodel-clients

## Release v0.2.0 - 2022-01-11(08:30:27 +0000)

### New

- implement ip client and ip mib

## Release v0.1.0 - 2021-12-09(10:47:18 +0000)

### New

- implement ip client and ip mib

